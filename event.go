package main

import (
	"encoding/json"
	"log"

	"github.com/garyburd/redigo/redis"
)

// event encodes all messages exchanged between the server and the client.
// All laravel events have a name and channel and so does the messages we
// receive from the client.
type event struct {
	Name          string                 `json:"event"`
	Channel       string                 `json:"channel"`
	IgnoredSocket string                 `json:"socket"`
	Data          map[string]interface{} `json:"data"`
}

// toJSON serializes the event toJSON. If there is an error we return an
// empty string.
func (ev event) toJSON() []byte {
	data, err := json.Marshal(ev)
	if err != nil {
		return []byte{}
	}
	return data
}

// subscribeToRedisQueue subscribes to events on the hub's redis connection. The
// events received are parsed and sent to the event's sockets. This function
// run on it's own goroutine, and foreach event received from redis it starts
// a goroutine to send the event.
func subscribeToRedisQueue(hub *hub) error {
	conn, err := hub.redisPool.Dial()
	if err != nil {
		return err
	}
	psc := redis.PubSubConn{Conn: conn}
	psc.PSubscribe("*")
	for {
		switch v := psc.Receive().(type) {
		case redis.PMessage:
			ev := event{}
			err := json.Unmarshal(v.Data, &ev)
			if err == nil {
				ev.Channel = v.Channel
				go hub.emit(ev)
			} else {
				log.Printf("error parsing redis event: %v - %v", err, v.Data)
			}
		case redis.Subscription:
			log.Printf("%s: %s %d\n", v.Channel, v.Kind, v.Count)
		case error:
			return v
		}
	}
}
