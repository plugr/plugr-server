package main

// subscription represents a connection between a socket and a channel with
// optional data.
type subscription struct {
	channel string
	socket  *socket
	data    map[string]interface{}
}
