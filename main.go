package main

import "log"

func main() {
	hub := newHub("test")
	hub.connectToRedis("localhost:6379")

	go subscribeToRedisQueue(hub)

	if err := startHTTPServer(hub); err != nil {
		log.Fatal(err)
	}
}
