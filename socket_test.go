package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestSubscribe(t *testing.T) {
	hub := newHub("test")
	socket := newSocket(hub)

	if err := socket.subscribe("lobby"); err != nil {
		t.Fatal(err)
	}

	channels := socket.subscriptions()
	expectedChannels := []string{"lobby"}
	if !reflect.DeepEqual(channels, expectedChannels) {
		t.Fatalf("expected socket to be subscribed to lobby")
	}
}

func TestSubscribeToPrivateChannelAllowed(t *testing.T) {
	// start fake server
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprint(w, "")
	}))
	defer ts.Close()

	// create hub and socket
	hub := newHub("test")
	socket := newSocket(hub)

	// force the hub to use our test server endpoint
	hub.laravelAuthURL = ts.URL

	// subscribe to private channel
	if err := socket.subscribe("private-lobby"); err != nil {
		t.Fatal(err)
	}

	// check the subscriptions is stored in the index
	if !socket.isSubscribedTo("private-lobby") {
		t.Fatal("expected socket to be subscribed to private-lobby")
	}
}

func TestSubscribeToPrivateChannelDenied(t *testing.T) {
	// start fake server
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(403)
		fmt.Fprint(w, "")
	}))
	defer ts.Close()

	// create hub and socket
	hub := newHub("test")
	socket := newSocket(hub)

	// force the hub to use our test server endpoint
	hub.laravelAuthURL = ts.URL

	// subscribe to private channel
	if err := socket.subscribe("private-lobby"); err != socketNotAuthorizedError {
		t.Fatal(err)
	}

	// check a message of failed subscription was sent to the socket
	// TODO

	// check the subscription is not stored in the index
	if socket.isSubscribedTo("private-lobby") {
		t.Fatal("socket should not be subscribed to channel after failed auth")
	}
}

func TestSubscribeToPresenceChannel(t *testing.T) {
	/*
		// Given we have a hub and a socket
		hub := newHub("test")
		socket := newSocket(hub)

		// And we initialize the connection to redis.
		hub.connectToRedis("localhost:6379")

		// And some data we're going to respond from the server
		userData := map[string]interface{}{
			"name":    "John Doe",
			"isAdmin": true,
		}

		// And we have a fake http server that allows the presence channel
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			data, _ := json.Marshal(userData)
			w.WriteHeader(200)
			fmt.Fprint(w, data)
		}))
		defer ts.Close()

		// And we force the hub to use the test server
		hub.laravelAuthURL = ts.URL

		// When we subscribe to a presence channel
		if err := socket.subscribe("presence-home"); err != nil {
			t.Fatal(err)
		}

		// Then the subscription should be stored on redis.
		conn := hub.redisPool.Get()
		defer conn.Close()

		data, err := conn.Do("HGET", hub.redisKey(), "presence-home")
		if err != nil {
			t.Fatal(err)
		}
		users, ok := data.([]byte)
		if !ok {
			t.Fatal("expected data from redis to be []byte")
		}

		var subscriptions []map[string]interface{}
		if err := json.Unmarshal(users, &subscriptions); err != nil {
			t.Fatal(err)
		}
		if len(subscriptions) != 1 {
			t.Fatalf("expected one subscription on redis, got %v", len(subscriptions))
		}
		if !reflect.DeepEqual(subscriptions[0], userData) {
			t.Fatalf("expected data on redis to be the same of our user data, got: %+v", subscriptions[0])
		}
	*/
}

func TestErrorOnMultipleSubscriptions(t *testing.T) {
	hub := newHub("test")
	socket := newSocket(hub)

	if err := socket.subscribe("lobby"); err != nil {
		t.Fatal(err)
	}

	if err := socket.subscribe("lobby"); err != socketAlreadySubscribedError {
		t.Fatal("expected error on second attempt to connect to the same channel")
	}
}

func TestUnsubscribe(t *testing.T) {
	hub := newHub("test")
	socket := newSocket(hub)

	if err := socket.subscribe("lobby"); err != nil {
		t.Fatal(err)
	}

	if err := socket.unsubscribe("lobby"); err != nil {
		t.Fatal(err)
	}

	channels := socket.subscriptions()
	expectedChannels := []string{}

	if !reflect.DeepEqual(channels, expectedChannels) {
		t.Fatal("expected socket to be subscribed to no channels")
	}
}

func TestUnsubscribeErrorIfNotSubscribed(t *testing.T) {
	hub := newHub("test")
	socket := newSocket(hub)

	if err := socket.unsubscribe("lobby"); err != socketNotSubscribedError {
		t.Fatal("expected unsubscribe to return an error")
	}
}

func TestUnsubscribeErrorIfCalledMultipleTimes(t *testing.T) {
	hub := newHub("test")
	socket := newSocket(hub)

	socket.subscribe("lobby")

	// first one is ok
	if err := socket.unsubscribe("lobby"); err != nil {
		t.Fatal(err)
	}

	// second one is not ok
	if err := socket.unsubscribe("lobby"); err != socketNotSubscribedError {
		t.Fatal("expected error when unsubscribing multiple times")
	}
}

func TestUnregister(t *testing.T) {
	hub := newHub("test")
	socket := newSocket(hub)

	socket.subscribe("lobby")

	socket.unregister()

	sockets := hub.socketsSubscribed("lobby")
	if len(sockets) != 0 {
		t.Fatal("expected unregistered socket to be removed from lobby")
	}

	channels := socket.subscriptions()
	if len(channels) != 0 {
		t.Fatal("expected unregistered socket to be removed from lobby")
	}
}
