package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPrivateChannel(t *testing.T) {
	if !isPrivateChannel("private-lobby") {
		t.Fatal("expected private-lobby to be private")
	}

	if isPrivateChannel("presence-lobby") {
		t.Fatal("expected presence-lobby not to be private")
	}

	if isPrivateChannel("lobby") {
		t.Fatal("expected lobby not to be private")
	}
}

func TestPresenceChannel(t *testing.T) {
	if !isPresenceChannel("presence-lobby") {
		t.Fatal("expected presence-lobby to be presence")
	}

	if isPresenceChannel("private-lobby") {
		t.Fatal("expected private-lobby not to be presence")
	}

	if isPresenceChannel("lobby") {
		t.Fatal("expected lobby not to be presence")
	}
}

func TestIsAuthorizedSuccess(t *testing.T) {
	// Given we have a server that responds with 200
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprint(w, "")
	}))
	defer ts.Close()

	// When we send a request to check the authorization
	ok, err := isAuthorizedToSubscribe(ts.URL, "any-channel", map[string]string{})
	if err != nil {
		t.Fatal(err)
	}

	// Then we expect to be authorized
	if !ok {
		t.Fatal("expect auth request to respond with 200 ok (authorized)")
	}
}

func TestIsAuthorizedFailed(t *testing.T) {
	// Given we have a server that responds with 403
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprint(w, "")
	}))
	defer ts.Close()

	// When we send a request to check the authorization
	ok, err := isAuthorizedToSubscribe(ts.URL, "any-channel", map[string]string{})
	if err != nil {
		t.Fatal(err)
	}

	// Then we expect not to be authorized
	if ok {
		t.Fatal("expected request to server to respond with unauthroized (and not 200 OK)")
	}
}
