package main

import (
	"math/rand"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var (
	// writeWait is the max time we have to write a message to a socket before
	// understanding it as an error.
	writeWait = 10 * time.Second
)

const (
	letterBytes   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	letterIdxBits = 6
	letterIdxMask = 1<<letterIdxBits - 1
	letterIdxMax  = 63 / letterIdxBits
)

// socket is the middleman between the websocket connection (provided by
// gorilla) and the hub (that manages the state of the server).
type socket struct {
	// Unique ID of the socket in this server.
	id string

	// Reference to the hub the socket belongs to.
	hub *hub

	// Headers we're gonna send to laravel so they know which user is requesting
	// a subscription.
	authHeaders map[string]string

	// Underlying wesocket connection provided by gorilla/websocket.
	conn *websocket.Conn

	// Mutex used to protect writes to the websocket connection.
	writeMutex sync.Mutex
}

// generateRandomSocketID returns a random string with n length. I have no
// idea how this function works, I just copied it from:
// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-golang
func generateRandomSocketID(n int) string {
	b := make([]byte, n)
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i > 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i -= 1
		}
		cache >>= letterIdxBits
		remain -= 1
	}

	return string(b)
}

// newSocket creates a new socket in the given hub.
func newSocket(hub *hub) *socket {
	socket := &socket{
		hub:         hub,
		id:          generateRandomSocketID(30),
		authHeaders: make(map[string]string),
	}

	return socket
}

// subscribe subscribes the socket to the given channel. It deleages it
// to the hub's subscribe function.
func (s *socket) subscribe(channel string) error {
	return s.hub.subscribe(s, channel)
}

// unsubscribe unsubscribes the socket from the given channel. It deleagates it
// to the hub's unsubscribe function.
func (s *socket) unsubscribe(channel string) error {
	return s.hub.unsubscribe(s, channel)
}

// unregister removes all subscriptions, triggering appropriate callbacks and
// redis serialization. See hub's unregister for more details.
func (s *socket) unregister() error {
	return s.hub.unregister(s)
}

// subscriptions returns the channels the socket is subscribed to. It
// delegates it to the hub sockets -> channels index.
func (s *socket) subscriptions() []string {
	return s.hub.channelsSubscribed(s)
}

// isSubscribedTo returns true if the socket is subscribed to `channel`, false
// otherwise.
func (s *socket) isSubscribedTo(channel string) bool {
	channels := s.subscriptions()
	for _, c := range channels {
		if c == channel {
			return true
		}
	}
	return false
}

// sendText sends the given data to the socket with the type TextMessage.
func (s *socket) sendText(data []byte) error {
	s.writeMutex.Lock()
	defer s.writeMutex.Unlock()

	if s.conn == nil {
		// If the underlying connection is nil it means that we're in a test
		// environment. So do nothing for now.
		return nil
	} else {
		s.conn.SetWriteDeadline(time.Now().Add(writeWait))
		return s.conn.WriteMessage(websocket.TextMessage, data)
	}
}

// sendPing sends a 'ping' message as described in the ref.
func (s *socket) sendPing() error {
	s.writeMutex.Lock()
	defer s.writeMutex.Unlock()

	s.conn.SetWriteDeadline(time.Now().Add(writeWait))
	return s.conn.WriteMessage(websocket.PingMessage, []byte{})
}
