package main

import (
	"testing"
)

func TestEmitSingleSocket(t *testing.T) {
	hub := newHub("test")
	socket := newSocket(hub)
	socket.subscribe("lobby")

	count, err := hub.emit(event{Name: "ev", Channel: "lobby"})

	if err != nil {
		t.Fatal(err)
	}
	if count != 1 {
		t.Fatal("expected event to be emitted to one socket")
	}
}

func TestEmitNoSockets(t *testing.T) {
	hub := newHub("test")

	count, err := hub.emit(event{Name: "test", Channel: "lobby"})
	if err != nil {
		t.Fatal(err)
	}
	if count != 0 {
		t.Fatal("expected event to be emitted to 0 sockets")
	}
}

func TestEmitMultipleSockets(t *testing.T) {
	hub := newHub("test")
	s1 := newSocket(hub)
	s1.subscribe("lobby")
	s2 := newSocket(hub)
	s2.subscribe("lobby")

	count, err := hub.emit(event{Name: "test", Channel: "lobby"})
	if err != nil {
		t.Fatal(err)
	}
	if count != 2 {
		t.Fatal("expected event to be emitted to 2 sockets")
	}
}

func TestEmitIgnoredSocket(t *testing.T) {
	hub := newHub("test")

	s1 := newSocket(hub)
	s1.subscribe("lobby")

	s2 := newSocket(hub)
	s2.subscribe("lobby")

	ev := event{Name: "something-happened", Channel: "lobby", IgnoredSocket: s1.id}
	count, err := hub.emit(ev)
	if err != nil {
		t.Fatal(err)
	}

	if count != 1 {
		t.Fatal("expected event to be emitted to sockets not ignored")
	}
}
