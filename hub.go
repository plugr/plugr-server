package main

import (
	"errors"
	"log"
	"sync"
	"time"

	"github.com/garyburd/redigo/redis"
)

var (
	// socketAlreadySubscribedError is returned from hub's `subscribe` function
	// if the socket is already subscribed to the channel.
	// If this sounds weird, remeber that a socket is a single connection and
	// not a single user. One user can have multiple sockets if they open
	// multiple tabs, for example.
	socketAlreadySubscribedError = errors.New("socket already subscribed to the channel")

	// socketNotSubscribedError is returned from hub's `unsubscribe` function
	// if a socket attempts to unsubcribe from a channel they're not subscribed.
	socketNotSubscribedError = errors.New("socket not subscribed to the channel")

	// socketNotAuthorizedError is returned from hub's 'subscribe' function if
	// Laravel app rejects the socket.
	socketNotAuthorizedError = errors.New("socket not authorized to subscribe")
)

type hub struct {
	// Name of the hub. Should be unique across the cluster.
	name string

	// Pool of connections to Redis. The connections in this pool are *only*
	// to write presence channels data to redis.
	redisPool *redis.Pool

	// Endpoint of our laravel app that allows/denies socket subscriptions to
	// channels.
	laravelAuthURL string

	// Mutex used in 'subscribe' and 'unsubscribe'. It protects both
	// 'channelSubscriptions' and 'socketSubscriptions'.
	mu sync.Mutex

	// Map of channel -> sockets subscribed to the channel. This map
	// should contain the same data of socketSubscriptions, but in a
	// reverse index.
	channelSubscriptions map[string][]subscription

	// Map of socket -> channels it is subscribed. This map should contain
	// the same data of channelSubscriptions, but in a reverse index.
	socketSubscriptions map[*socket][]subscription
}

// newHub returns a hub with empty connections.
func newHub(name string) *hub {
	return &hub{
		name:                 name,
		channelSubscriptions: make(map[string][]subscription),
		socketSubscriptions:  make(map[*socket][]subscription),
	}
}

// redisKey returns the key used to store the hub's subscriptions on redis.
func (h hub) redisKey() string {
	return "plugr:" + h.name + ":subscriptions"
}

// connectToRedis defines the redis pool configuration to the given address.
func (h *hub) connectToRedis(addr string) {
	h.redisPool = &redis.Pool{
		// If Wait is true and pool is at the max capacity, calling Get() waits
		// for a connection to be returned.
		Wait: true,

		// Max of connections active at the same time.
		MaxActive: 3,

		// Definition of the connection logic to redis. This function will be
		// called by other parts of the system, such as event's PSUBSCRIBE.
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr)
		},

		// PINGs connections that have been idle more than a minute to check
		// the health.
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}

			_, err := c.Do("PING")
			return err
		},
	}
}

// subscribe subscribes the socket to the given channel. It stores the
// subscription in an in-memory index as well as redis if the channel
// is of the type presence. An error is returned if the socket is already
// subscribed to the channel.
func (h *hub) subscribe(s *socket, channel string) error {
	if isPrivateChannel(channel) || isPresenceChannel(channel) {
		ok, err := isAuthorizedToSubscribe(h.laravelAuthURL, channel, s.authHeaders)
		if err != nil {
			return err
		}
		if !ok {
			return socketNotAuthorizedError
		}
	}

	h.mu.Lock()
	defer h.mu.Unlock()

	sub := subscription{
		channel: channel,
		socket:  s,
	}

	if val, ok := h.socketSubscriptions[s]; ok {
		for _, c := range val {
			if c.channel == channel {
				return socketAlreadySubscribedError
			}
		}

		h.socketSubscriptions[s] = append(val, sub)
	} else {
		h.socketSubscriptions[s] = []subscription{sub}
	}

	if val, ok := h.channelSubscriptions[channel]; ok {
		h.channelSubscriptions[channel] = append(val, sub)
	} else {
		h.channelSubscriptions[channel] = []subscription{sub}
	}

	if isPresenceChannel(channel) {
		h.updateSubscriptionsOnRedis(channel)
	}

	return nil
}

func (h *hub) updateSubscriptionsOnRedis(channel string) error {
	return nil
}

// unsubscribe unsubscribes the socket from the given channel. It clears
// the subscription from the in-memory index as well as redis if the
// channel is of the type presence. An error is returned if the socket
// not subscribed to the channel.
func (h *hub) unsubscribe(s *socket, channel string) error {
	h.mu.Lock()
	defer h.mu.Unlock()

	// remove the channel from the socket's subscriptions
	if val, ok := h.socketSubscriptions[s]; ok {
		socketRemoved := false

		for i, c := range val {
			if c.channel == channel {
				socketRemoved = true
				val[len(val)-1], val[i] = val[i], val[len(val)-1]
				h.socketSubscriptions[s] = val[:len(val)-1]
				break
			}
		}

		if !socketRemoved {
			return socketNotSubscribedError
		}
	} else {
		return socketNotSubscribedError
	}

	// remove the socket from the channels's subscriptions
	if val, ok := h.channelSubscriptions[channel]; ok {
		socketRemoved := false

		for i, sub := range val {
			if sub.socket == s {
				socketRemoved = true
				val[len(val)-1], val[i] = val[i], val[len(val)-1]
				h.channelSubscriptions[channel] = val[:len(val)-1]
				break
			}
		}

		if !socketRemoved {
			return socketNotSubscribedError
		}
	} else {
		return socketNotSubscribedError
	}

	return nil
}

// unregister removes the socket from all channels, triggering appropriate
// callbacks and redis serialization
func (h *hub) unregister(s *socket) error {
	channels := h.channelsSubscribed(s)
	var err error = nil
	for _, channel := range channels {
		unsuberr := h.unsubscribe(s, channel)
		if unsuberr != nil && err == nil {
			err = unsuberr
		}
	}
	return err
}

// emit sends the event to the sockets subscribed to the event channel,
// excluding the event's socket if specified (toOthers).
func (h *hub) emit(ev event) (int, error) {
	sockets := h.socketsSubscribed(ev.Channel)
	count := 0
	eventSerialized := ev.toJSON()

	for _, socket := range sockets {
		if ev.IgnoredSocket != "" && socket.id == ev.IgnoredSocket {
			continue
		}
		count += 1
		socket.sendText(eventSerialized)
	}

	log.Printf("event %v sent to %d sockets", ev.Name, count)

	return count, nil
}

// socketsSubscribed returns the sockets subscribed to the channel. An
// empty list of returned if no socket is subscribed to the channel.
func (h *hub) socketsSubscribed(channel string) []*socket {
	if subs, ok := h.channelSubscriptions[channel]; ok {
		sockets := make([]*socket, len(subs))
		for i, sub := range subs {
			sockets[i] = sub.socket
		}
		return sockets
	}
	return []*socket{}
}

// channelsSubscribed returns the channels the socket is subscribed to.
// All channels are returned, including private and presence channels.
func (h *hub) channelsSubscribed(s *socket) []string {
	if subs, ok := h.socketSubscriptions[s]; ok {
		channels := make([]string, len(subs))
		for i, sub := range subs {
			channels[i] = sub.channel
		}
		return channels
	}
	return []string{}
}
