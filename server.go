package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

var (
	pongWait     = 5 * time.Second
	pingInterval = (pongWait * 9) / 10
)

// serverStats returns some stats about the server, such as number of
// goroutines and memory consumption.
func serverStatsHandler(w http.ResponseWriter, r *http.Request) {
	stats := snapshotServerStats()
	data, err := json.Marshal(stats)
	if err != nil {
		log.Fatal(err)
		fmt.Fprintf(w, "%v", err)
		return
	}
	fmt.Fprint(w, string(data))
}

func socketStatsHandler(hub *hub) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		data, err := json.Marshal(hub.channelSubscriptions)
		log.Printf("%+v", hub.channelSubscriptions)
		if err != nil {
			log.Fatal(err)
			fmt.Fprintf(w, "%v", err)
			return
		}
		fmt.Fprint(w, string(data))
	}
}

// readMessages runs on it's own goroutine. It reads message from the socket,
// call the proper handelrs and closes the connection at the end. It also
// manages Pong messages in order to keep the connection alive.
func readSocketMessages(s *socket) {
	defer func() {
		s.unregister()
		s.conn.Close()
	}()

	// Set read deadline for the first "ping" message.
	s.conn.SetReadDeadline(time.Now().Add(pongWait))

	// Callback called when the peer responds the ping message. We update
	// the expected deadline for us to receive the next message.
	s.conn.SetPongHandler(func(string) error {
		log.Println("pong handler!")
		s.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		msgtype, msg, err := s.conn.ReadMessage()

		if err != nil {
			log.Printf("read message error: %v", err)
			break
		}

		if msgtype == websocket.TextMessage {
			ev := event{}
			err := json.Unmarshal(msg, &ev)
			if err == nil {
				if ev.Name == "subscribe" {
					log.Printf("subscribing to channel %v", ev.Channel)

					// We're running the 'subscribe' in a goroutine because
					// we might send a HTTP request to laravel.
					go s.subscribe(ev.Channel)
				} else if ev.Name == "unsubscribe" {
					s.unsubscribe(ev.Channel)
				} else {
					log.Printf("don't know how to handle event: %v", ev.Name)
				}
			} else {
				log.Printf("failed to parse msg from client: %v", string(msg))
			}
		} else {
			log.Printf("don't know how to handle msgs other than text [%v]: %v",
				msgtype, msg)
		}
	}
}

func writeSocketMessages(s *socket) {
	ticker := time.NewTicker(pingInterval)

	defer func() {
		ticker.Stop()
		s.conn.Close()
	}()

	for {
		<-ticker.C
		if err := s.sendPing(); err != nil {
			log.Printf("ping error: %v", err)
			return
		}
	}
}

// websocketHandler negotiates the handshake and establishes a websocket
// connection. From what I understood from the docs we don't need to respond
// anything to the HTTP request, the upgrader already takes care of it.
func websocketHandler(hub *hub) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("error on protocol upgrade: %v", err)
			fmt.Fprintf(w, "%v", err)
			return
		}

		// create the socket with the underlying connection
		socket := newSocket(hub)
		socket.conn = conn

		log.Println("new connection!")

		go readSocketMessages(socket)
		go writeSocketMessages(socket)
	}
}

// startHTTPServer starts the HTTP server loop. It shouldn't return.
func startHTTPServer(hub *hub) error {
	http.HandleFunc("/", websocketHandler(hub))
	http.HandleFunc("/stats/server", serverStatsHandler)
	http.HandleFunc("/stats/socket", socketStatsHandler(hub))

	log.Printf("HTTP server started on port :8080")
	return http.ListenAndServe(":8080", nil)
}
