package main

import (
	"net/http"
	"strings"
)

// isPrivateChannel returns true if the given channel is private. Private
// channels are prefixed with "private-".
func isPrivateChannel(c string) bool {
	return strings.Index(c, "private-") == 0
}

// isPresenceChannel returns true if the given channel is presence. Presence
// channels are prefixed with "presence-".
func isPresenceChannel(c string) bool {
	return strings.Index(c, "presence-") == 0
}

// isAuthorizedToSubscribe sends a HTTP request to `url` to check if the
// user can subscribe to the channel. The Laravel app knows which user it
// is by the headers, specificaly "x-csrf-token".
func isAuthorizedToSubscribe(url string, c string, headers map[string]string) (bool, error) {
	req, err := http.NewRequest("POST", url, nil)
	req.Header.Set("content-type", "application/json")

	for key, val := range headers {
		req.Header.Set(key, val)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	// If the server responded with 200 OK, the user is authorized to connect
	// to the channel. All other status denies it.
	if resp.StatusCode == 200 {
		return true, nil
	} else {
		return false, nil
	}
}
