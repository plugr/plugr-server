package main

import "runtime"

type serverStats struct {
	NumCPU        int
	NumGoroutines int
	Alloc         uint64
	TotalAlloc    uint64
	HeapAlloc     uint64
	HeapSys       uint64
}

func snapshotServerStats() serverStats {
	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)

	return serverStats{
		NumCPU:        runtime.NumCPU(),
		NumGoroutines: runtime.NumGoroutine(),
		Alloc:         mem.Alloc,
		TotalAlloc:    mem.TotalAlloc,
		HeapAlloc:     mem.HeapAlloc,
		HeapSys:       mem.HeapSys,
	}
}
